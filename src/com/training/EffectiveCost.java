package com.training;
/**
 * 
 * @author nikitajain
 *
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import com.training.models.Item;
import com.training.views.InputAcceptor;
import com.training.views.Output;
public class EffectiveCost {
    
    public static void main(String[] args) throws IOException {
      ArrayList<Item> items = new ArrayList<Item>();
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      char quit;
      Item item;
      double effectivePrice=0.0;
      double totalPrice;
      do {
          item = InputAcceptor.takeInput();
          item.calculateSalesTax();
          totalPrice = item.getQuantity()*item.getFinalPrice();
          item.setTotalPrice(totalPrice);
          effectivePrice+=item.getTotalPrice();
          items.add(item);
          System.out.println("Do you want to enter details of any other item (y/n):");
          quit=br.readLine().charAt(0);
      }while((quit=='y') || (quit=='Y'));
      System.out.println("Following is the summary of items-");
      for (Item i: items) {
          Output.displayOutput(i);
          System.out.println("");
      }
      System.out.println("Effective Price = " + effectivePrice);
    }
}
