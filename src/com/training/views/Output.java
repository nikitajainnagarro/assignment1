package com.training.views;
/**
 * 
 * @author nikitajain
 *
 */
import com.training.models.Item;

public class Output {
    public static void displayOutput(Item item) {
        String name = item.getName();
        double price = item.getPrice();
        double salesTaxLiability = item.getSalesTax();
        double finalPrice=item.getFinalPrice();
        System.out.print("Name:" + name + ", Price:"+ price+ ", Sales tax Liability: "+salesTaxLiability + ", Final Price:" + finalPrice);
    }
}
