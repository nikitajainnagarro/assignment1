package com.training.views;
/**
 * 
 * @author nikitajain
 *
 */
import com.training.models.Const;

public class InputValidator {
    public static boolean checkItemPrice(String inp) {
        try {
            Double.parseDouble(inp);
        }
        catch (NumberFormatException e) {
            System.err.print("Please enter price upto two decimal places: ");
            return false;
        }
        return true;
    }
    
    public static boolean checkItemType(String inp) {
        if(inp.equalsIgnoreCase(Const.RAW)|inp.equalsIgnoreCase(Const.MANUFACTURED)|inp.equalsIgnoreCase(Const.IMPORTED)) {
            return true;
        }
        System.err.print("Please enter one of the 3 types(raw/manufactured/imported)");
        return false;
    }
    
    public static boolean checkItemQuantity(String inp) {
        try {
            Integer.parseInt(inp);
        }
        catch (NumberFormatException e) {
            System.err.print("Please enter quantity as a whole number: ");
            return false;
        }
        return true;
    }
}
