package com.training.views;
/**
 * 
 * @author nikitajain
 *
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.training.models.Const;
import com.training.models.Item;
import com.training.models.ItemImported;
import com.training.models.ItemManufactured;
import com.training.models.ItemRaw;

public class InputAcceptor {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    public static Item takeInput() throws IOException {
        String name;
        double price;
        int quantity;
        String type;
        String inp;
        boolean isValid;
        
        System.out.println("Enter the Item details...");
        System.out.println("Name: ");
        name = br.readLine();
        
        System.out.println("Price: ");
        do {
            inp = br.readLine();
            isValid = InputValidator.checkItemPrice(inp);
        }while(!isValid);
        price = Double.parseDouble(inp);
        
        System.out.println("Type(raw/manufactured/imported): ");
        do {
            inp = br.readLine();
            isValid = InputValidator.checkItemType(inp);
        }while(!isValid);
        type = inp.toLowerCase();
        
        System.out.println("Quantity: ");
        do {
            inp = br.readLine();
            isValid = InputValidator.checkItemQuantity(inp);
        }while(!isValid);
        quantity = Integer.parseInt(inp);
        
        Item item = null;
        switch (type) {
            case Const.RAW:
                item = new ItemRaw(name, price, quantity, type);
                break;
            case Const.MANUFACTURED:
                item = new ItemManufactured(name, price, quantity, type);
                break;
            case Const.IMPORTED:
                item = new ItemImported(name, price, quantity, type);
                break;
        }
        return item;
    }
}
