package com.training.models;
/**
 * 
 * @author nikitajain
 *
 */
public class ItemImported extends Item{
    public ItemImported (String name, double price,int quantity, String type) {
        super(name, price, quantity, type );
    }
    
    public void calculateSalesTax() {
        salesTaxLiability = Const.IMPORT_DUTY_IMPORTED * price;
        finalPrice = price + salesTaxLiability;
        double surchargeRateImported;
        if(finalPrice<=100) {
            surchargeRateImported = 5;
        }
        else if(finalPrice<=200) {
            surchargeRateImported = 10;
        }
        else {
            surchargeRateImported = 0.05 * finalPrice;
        }
        salesTaxLiability+=surchargeRateImported;
        finalPrice+=surchargeRateImported;
    }
}
