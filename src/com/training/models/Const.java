package com.training.models;
/**
 * 
 * @author nikitajain
 *
 */
public class Const {
    public final static String RAW = "raw";
    public final static String MANUFACTURED = "manufactured";
    public final static String IMPORTED = "imported";
    public final static double TAX_RATE_RAW = 0.125;
    public final static double TAX_RATE_MANUFACTURED = 0.125;
    public final static double SURCHARGE_RATE_MANUFACTURED = 0.02;
    public final static double IMPORT_DUTY_IMPORTED = 0.1;
}
