package com.training.models;
/**
 * 
 * @author nikitajain
 *
 */
public abstract class Item {
    String name;
    double price;
    int quantity;
    String type; 
    double salesTaxLiability;
    double finalPrice;
    double totalPrice;
    
    public Item(String name,double price, int quantity, String type) {
        this.name=name;
        this.price=price;
        this.quantity=quantity; 
        this.type=type;
    }
    
    public abstract void calculateSalesTax();
    
    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }

    public void setPrice(double price){
        this.price=price;
    }
    public double getPrice(){
        return price;
    }

    public void setQuantity(int quantity){
        this.quantity=quantity;
    }
    public int getQuantity(){
        return quantity;
    }

    public void setType(String type){
        this.type=type;
    }
    public String getType(){
        return type;
    }
    
    public double getSalesTax() {
        return salesTaxLiability;
    }
    
    public double getFinalPrice() {
        return finalPrice;
    }
    
    public void setTotalPrice(double totalPrice){
        this.totalPrice=totalPrice;
    }
    public double getTotalPrice() {
        return totalPrice;
    }
}