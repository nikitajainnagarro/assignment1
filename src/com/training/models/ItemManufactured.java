package com.training.models;
/**
 * 
 * @author nikitajain
 *
 */
public class ItemManufactured extends Item{
    public ItemManufactured (String name, double price,int quantity, String type) {
        super(name, price, quantity, type );
    }
    
    public void calculateSalesTax() {
        double manufacturedTax = Const.TAX_RATE_MANUFACTURED;
        double manufacturedSurcharge = Const.SURCHARGE_RATE_MANUFACTURED;
        salesTaxLiability = (manufacturedTax + manufacturedSurcharge + manufacturedTax * manufacturedSurcharge) * price;
        finalPrice = price + salesTaxLiability;
    }
}
