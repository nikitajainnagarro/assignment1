package com.training.models;
/**
 * 
 * @author nikitajain
 *
 */
public class ItemRaw extends Item{
    public ItemRaw (String name, double price,int quantity, String type) {
        super(name, price, quantity, type );
    }
    
    public void calculateSalesTax() {
        salesTaxLiability = Const.TAX_RATE_RAW * price;
        finalPrice = price + salesTaxLiability;
    }
}
